FROM kasmweb/core-ubuntu-bionic:1.10.0
USER root

ENV HOME /home/kasm-default-profile
ENV STARTUPDIR /dockerstartup
ENV INST_SCRIPTS $STARTUPDIR/install
WORKDIR $HOME

######### Customize Container Here ###########

#FROM gcr.io/projectsigstore/cosign:v1.9.0 as cosign-bin

# Source: https://github.com/distroless/static
#FROM ghcr.io/distroless/static:latest
#COPY --from=cosign-bin /ko-app/cosign /usr/local/bin/cosign
#ENTRYPOINT [ "cosign" ]

# RUN mkdir /usr/local/bin/sift \
#    && cd /usr/local/bin/sift \
#    && wget https://github.com/teamdfir/sift-cli/releases/download/v1.14.0-rc1/sift-cli-linux \
#    && wget https://github.com/teamdfir/sift-cli/releases/download/v1.14.0-rc1/sift-cli-linux.sig \
#    && wget https://github.com/teamdfir/sift-cli/releases/download/v1.14.0-rc1/sift-cli.pub \
#    && chmod 755 /usr/local/bin/sift
#    && cosign verify-blob --key sift-cli.pub --signature sift-cli-linux.sig sift-cli-linux \
	
# CMD ["sift", "install"] 

RUN apt-get update
RUN apt-get upgrade -y
RUN apt-get install curl sudo wget -y

RUN curl -Lo /usr/local/bin/sift https://github.com/teamdfir/sift-cli/releases/download/v1.14.0-rc1/sift-cli-linux
RUN chmod +x /usr/local/bin/sift
RUN chmod 755 /usr/local/bin/sift

RUN adduser --disabled-password --gecos '' siftuser
RUN adduser siftuser sudo
RUN echo '%sudo ALL=(ALL) NOPASSWD:ALL' >> /etc/sudoers

USER siftuser
RUN sudo apt-get update || :
RUN sudo sift install || :



######### End Customizations ###########

RUN chown 1000:0 $HOME
RUN $STARTUPDIR/set_user_permission.sh $HOME

ENV HOME /home/kasm-user
WORKDIR $HOME
RUN mkdir -p $HOME && chown -R 1000:0 $HOME

USER siftuser
#USER 1000
